<?php


use App\Http\Controllers\Api\AppointmentController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ContactsController;
use App\Http\Controllers\Api\NewPasswordController;
use App\Http\Controllers\Api\RegisterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('register', [RegisterController::class, 'register']);
Route::post('forgot-password', [NewPasswordController::class, 'forgotPassword']);
Route::post('reset-password', [NewPasswordController::class, 'reset'])->name('password.reset');
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

});
Route::group([
    'middleware' => 'auth.jwt',
    'prefix' => 'appointments'
], function () {
    Route::get('list', [AppointmentController::class, 'index']);
    Route::post('create', [AppointmentController::class, 'create']);
    Route::put('update/{id}', [AppointmentController::class, 'update']);
    Route::delete('delete/{id}', [AppointmentController::class, 'destroy']);

});
Route::group([
    'middleware' => 'auth.jwt',
    'prefix' => 'contacts'
], function () {
    Route::get('list', [ContactsController::class, 'index']);
});
Route::group([
    'middleware' => 'admin',
    'prefix' => 'admin'
], function () {
    //you can add routes here that only admins can use.
});

