<?php
return [
    'googlemaps_key' =>  env('GOOGLEMAPS_API'),
    'office_postcode' => env('OFFICE_POSTCODE', 'cm27pj')
    ];
