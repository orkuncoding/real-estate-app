
# Real Estate API

This API made for real estate agents to follow appointment schedule of their employees.
Real estate agents can see that how much time their employees are spending for each appointment via Google Maps API.


## Requirements

PHP version 8.0.2 or above
## Installation



```bash 
  composer install --ignore-platform-reqs
  cp .env.example .env
  php artisan key:generate
  php artisan migrate
  php artisan jwt:secret
```

## Installation Note

```bash 
 You must set to your SMTP credentials to send password reset mails.
 You must set to your Google Maps API in your env file.
 You must set to your office zip code in your env file.
 You should add your details as follow:

 GOOGLEMAPS_API=
 OFFICE_POSTCODE=
```
## API Usage

#### Register

First registered user will be admin by default.

```http
  POST /api/register
  -H "Accept: application/json"
```

| Parameter | Type   | Description               |
| :-------- | :------- | :------------------------- |
| `name` | `string` |   |
| `email` | `email` |   |
| `phone` | `string` |   |
| `password` | `string` |   |
| `password_confirmation` | `string` |  Must match with password |
| `office_code` | `string` |  *optinal and only for first registerer user |


#### Login

```http
  POST /api/auth/login
   -H "Accept: application/json"
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `email` | `email` |   |
| `password` | `string` |   |

#### Logout

```http
  POST /api/auth/logout
   -H "Accept: application/json"
  -H "Authorization: Bearer {Token}" 
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
|  |            |   ***API Token Required***|   

#### Forgot Password

```http
  POST /api/forgot-password
   -H "Accept: application/json"
 
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `email` | `email` |   |

#### Reset Password

```http
  POST /api/reset-password
   -H "Accept: application/json"
 
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `email` | `email` |   |
| `Token` | `token` |   **It was sent to your mail**|

#### Appointment List

```http
  GET /api/appointments/list
   -H "Accept: application/json"
  -H "Authorization: Bearer {Token}
 
```
It lists all ongoing and upcoming appointments order by ASC


| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `start` | `date dd/mm/yy` | lists all appointments from that date to the creation date. |
| `end` | `date dd/mm/yy` | lists all appointments from creation date to the that date. |
| `show` | `string` | lists all appointments in the database. |
| `orderBy` | `string` | lists appointments orderBy asc or desc. Your parameter must equal to asc or desc |

#### Appointment Create

```http
  POST /api/appointments/create
   -H "Accept: application/json"
   -H "Authorization: Bearer {Token}
 
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `name` | `string` |   Customer name|
| `surname` | `string` |   Customer surname|
| `phone` | `string` |   Customer phone|
| `email` | `email` |   Customer email(nullable)|
| `date` | `date format:dd/mm/yyyy H:i`   |   Appointment Date format dd/mm/yyyy H:i Example: 27/07/2021 14:30 |
| `address` | `string` |   Appointment adress zipcode|

#### Appointment Update

```http
  UPDATE /api/appointments/update/{id}
   -H "Accept: application/json"
   -H "Authorization: Bearer {Token}
 
```

| Parameter | Type   | Description               |
| :-------- | :------- | :-------------------------------- |
| `name` | `string` |   Customer name|
| `surname` | `string` |   Customer surname|
| `phone` | `string` |   Customer phone|
| `email` | `email` |   Customer email(nullable)|
| `date` | `date format:dd/mm/yyyy H:i`   |   Appointment Date format dd/mm/yyyy H:i Example: 27/07/2021 14:30 |
| `address` | `string` |   Appointment adress zipcode|

#### Appointment Delete

```http
  DELETE /api/appointments/delete/{id}
   -H "Accept: application/json"
   -H "Authorization: Bearer {Token}
 
```


  
