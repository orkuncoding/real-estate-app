<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'customer_name',
        'customer_surname',
        'customer_email',
        'customer_phone',
        'appointment_address',
        'appointment_distance',
        'appointment_date',
        'est_departure_time',
        'est_arrive_time'
    ];
    protected $casts = [
        'appointment_date' => 'datetime',
        'est_departure_time' => 'datetime',
        'est_arrive_time' => 'datetime'
    ];

    public function appointmentUser(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
