<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * @var mixed
     */


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'Appointment ID' => $this->id,
            'Employee Name Surname' => $this->appointmentUser->name,
            'Employee Phone' => $this->appointmentUser->phone,
            'Customer Name Surname' => $this->customer_name." ".$this->customer_surname,
            'Customer Phone' => $this->customer_phone,
            'Customer Email' =>  ($this->customer_email == null) ? 'none':$this->customer_email,
            'Appointment Address' => $this->appointment_address,
            'Appointment Date' => Carbon::parse($this->appointment_date)->format('d/m/Y H:i'),
            'Appointment Distance' => $this->appointment_distance,
            'Employee Departure Date' => Carbon::parse($this->est_departure_time)->format('d/m/Y H:i'),
            'Employee Arrive Date' => Carbon::parse($this->est_arrive_time)->format('d/m/Y H:i'),

        ];
    }
}
