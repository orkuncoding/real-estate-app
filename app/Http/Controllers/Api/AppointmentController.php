<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentCreateRequest;
use App\Http\Requests\AppointmentRequest;
use App\Http\Resources\AppointmentResource;
use App\Models\Appointment;
use App\Models\Contact;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AppointmentController extends Controller
{

    /**
     *
     * @return JsonResponse
     */
    public function index(AppointmentRequest $request)
    {

        $show = $request->show;
        $orderby = orderByCheck($request->orderBy);

        if($show == null){
            $created_start = $request->start;

            if($created_start != null){
                $created_start = Carbon::createFromFormat('d/m/Y', $request->start);
                if($request->end == null){

                    $created_end = Carbon::NOW();
                }else{
                    $created_end = Carbon::createFromFormat('d/m/Y', $request->end);
                }

                $appointments = Appointment::whereBetween('created_at', [$created_start, $created_end])->with('appointmentUser')->orderBy('appointment_date', $orderby)->get();
            }else{
                $appointments = Appointment::where('est_arrive_time', '>', Carbon::NOW())->with('appointmentUser')->orderBy('appointment_date', $orderby)->get();

            }
        }else{
            if($show == "all"){
                $appointments = Appointment::with('appointmentUser')->orderBy('appointment_date', $orderby)->get();
            }else{
                $appointments = Appointment::with('appointmentUser')->orderBy('appointment_date', $orderby)->get();
            }

        }

        if($appointments->isEmpty()){
            return response()->json(
                ['message' => "There is not any appointment."]
            );
        }else{
            return response()->json(AppointmentResource::collection($appointments));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(AppointmentCreateRequest $request)
    {
        $appointment_date =  Carbon::createFromFormat('d/m/Y H:i', $request->date);
        $appointments = Appointment::where('user_id', '=', auth()->user()->id)->select('est_departure_time', 'est_arrive_time')->get();
        foreach ($appointments as $appointment){
            $dep_time = Carbon::parse($appointment->est_departure_time);
            $arr_time = Carbon::parse($appointment->est_arrive_time);
            if($appointment_date->between($dep_time, $arr_time)){
                return response()->json([
                    'message' => 'You have another appointment that date.'
                ]);
            }
        }

        $googlemaps = calcDistance($request->address);
        if($googlemaps['status'] != "OK"){
            return response()->json([
                'error' => $googlemaps['message']
            ]);
        }
        $distance = $googlemaps['distance'];
        $est_departure_time = Carbon::createFromFormat('d/m/Y H:i', $request->date)->subSeconds($googlemaps['durationsecond']);

        $googlemap_arrive = calcDistanceArrive($request->address);
        $est_arrive_time = Carbon::createFromFormat('d/m/Y H:i', $request->date)->addSeconds($googlemap_arrive['durationsecond']+3600);

         Contact::updateOrCreate([
            'name' => $request->name,
            'surname' => $request->surname,
            'phone' => $request->phone,
            'email' => $request->email
        ]);
;

       $createappointment = Appointment::create([
            'user_id' => auth()->user()->id,
            'customer_name' => $request->name,
            'customer_surname' => $request->surname,
            'customer_phone' => $request->phone,
            'customer_email' => $request->email,
            'appointment_address' => $request->address,
            'appointment_date' => $appointment_date,
            'appointment_distance' => $distance,
            'est_departure_time' => $est_departure_time,
            'est_arrive_time' => $est_arrive_time
        ]);

        return response()->json(new AppointmentResource($createappointment));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AppointmentCreateRequest $request, $id)
    {
        $googlemaps = calcDistance($request->address);
        $distance = $googlemaps['distance'];
        $est_departure_time = Carbon::createFromFormat('d/m/Y H:i', $request->date)->subSeconds($googlemaps['durationsecond']);
        $googlemap_arrive = calcDistanceArrive($request->address);
        $est_arrive_time = Carbon::createFromFormat('d/m/Y H:i', $request->date)->addSeconds($googlemap_arrive['durationsecond']+3600);
        $appointment = Appointment::where('id', $id)->first();
        $appointment->update([
                 'user_id' => auth()->user()->id,
                'customer_name' => $request->name,
                'customer_surname' => $request->surname,
                'customer_phone' => $request->phone,
                'customer_email' => $request->email,
                'appointment_address' => $request->address,
                'appointment_date' => Carbon::createFromFormat('d/m/Y H:i', $request->date)->format('Y-m-d H:i'),
                'appointment_distance' => $distance,
                'est_departure_time' => $est_departure_time,
                'est_arrive_time' => $est_arrive_time
        ]);
        $data = [
            'message' => 'Appointment updated successfully',
            'appointment' =>  new AppointmentResource($appointment)
        ];
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {

        $appointment = Appointment::where('id', $id)->first();
        if($appointment){
            $appointment->delete();
            $data = [
                'message' => 'Appointment deleted successfully',
            ];
        }else{
            $data = [
                'message' => 'Appointment not found',
            ];
        }

        return response()->json($data);
    }
}
