<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactResource;
use App\Models\Contact;
use http\Env\Request;
use Illuminate\Http\JsonResponse;

class ContactsController extends Controller
{

    /**
     *
     * @return JsonResponse
     */
    public function index(){
        $contacts = Contact::all();

        return response()->json(ContactResource::collection($contacts));
    }
}
