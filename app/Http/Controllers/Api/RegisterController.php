<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request){


         $users = User::all();
        if($users->isEmpty()){
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'is_admin' => true,
                'password' => Hash::make($request->password),
                'office_postcode' => $request->office_postcode
            ]);
        }else{
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'is_admin' => false,
                'password' => Hash::make($request->password),
            ]);
        }


        return response()->json("Your account created successfully");

    }
}
