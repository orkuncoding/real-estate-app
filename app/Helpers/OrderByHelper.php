<?php
if(!function_exists('orderByCheck'))
{
    function orderByCheck($orderby){
        if($orderby != null){
            $lower= mb_strtolower($orderby);
        }else{
            $lower= "asc";
        }

        if($lower == "asc"){
            return "ASC";
        }elseif ($lower == "desc"){
            return "DESC";
        }else{
            return "ASC";
        }
    }
}
