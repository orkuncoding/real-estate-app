<?php

use Illuminate\Support\Facades\Http;

if(!function_exists('calcDistance'))
{
    function calcDistance($destination){
        $googlemaps = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations='.$destination.'&origins='.config('custom.office_postcode').'&language=en&region=uk&units=imperial&mode=driving&key='.config('custom.googlemaps_key').'');
        $result = json_decode($googlemaps);
        $distance = $result->rows[0]->elements[0]->distance->text;
        $distancemeter = $result->rows[0]->elements[0]->distance->value;
        $duration = $result->rows[0]->elements[0]->duration->text;
        $durationsecond = $result->rows[0]->elements[0]->duration->value;
        $data = [
            'distance' => $distance,
            'distancemeter' => $distancemeter,
            'duration' => $duration,
            'durationsecond' => $durationsecond
        ];
        return $data;
    }
}
if(!function_exists('calcDistanceArrive')){
    function calcDistanceArrive($destination){
        $googlemaps = Http::withHeaders([
            'Accept' => 'application/json',
        ])->get('https://maps.googleapis.com/maps/api/distancematrix/json?destinations='.config('custom.office_postcode').'&origins='.$destination.'&language=en&region=uk&units=imperial&mode=driving&key='.config('custom.googlemaps_key').'');
        $result = json_decode($googlemaps);
        $status = $result->status;
        if($status == "OK"){
            $distance = $result->rows[0]->elements[0]->distance->text;
            $distancemeter = $result->rows[0]->elements[0]->distance->value;
            $duration = $result->rows[0]->elements[0]->duration->text;
            $durationsecond = $result->rows[0]->elements[0]->duration->value;
            $data = [
                'status' => 'OK',
                'distance' => $distance,
                'distancemeter' => $distancemeter,
                'duration' => $duration,
                'durationsecond' => $durationsecond
            ];
            return $data;
        }else{
            $data = [
                'status' => 'FAILED',
                'message' => $result->error_message
            ];
            return $data;
        }

    }
}
