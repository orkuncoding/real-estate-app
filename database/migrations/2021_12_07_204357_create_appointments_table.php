<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('customer_name');
            $table->string('customer_surname');
            $table->string('customer_phone');
            $table->string('customer_email')->nullable();
            $table->string('appointment_address');
            $table->string('appointment_distance');
            $table->dateTime('appointment_date');
            $table->dateTime('est_departure_time');
            $table->dateTime('est_arrive_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
